//
//  ViewController.swift
//  drop_down_menu
//
//  Created by Md Khaled Hasan Manna on 27/11/20.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var departmentBtnCollection: [UIButton]!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
    }


    @IBAction func departmentAction(_ sender: UIButton) {
        
        departmentBtnCollection.forEach { (button) in
            UIView.animate(withDuration: 0.3) {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            }
            
        }
        
    }
}

